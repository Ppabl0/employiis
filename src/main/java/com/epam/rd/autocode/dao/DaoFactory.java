package com.epam.rd.autocode.dao;

public class DaoFactory {
    public EmployeeDao employeeDAO() {
        return new EmployeeDaoSourceCode();

    }

    public DepartmentDao departmentDAO() {
        return new DepartmentDaoSourceCode();
    }
}
