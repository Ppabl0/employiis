package com.epam.rd.autocode.dao;

import com.epam.rd.autocode.ConnectionSource;
import com.epam.rd.autocode.domain.Department;
import com.epam.rd.autocode.domain.Employee;
import com.epam.rd.autocode.domain.FullName;
import com.epam.rd.autocode.domain.Position;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DepartmentDaoSourceCode implements DepartmentDao{

    ConnectionSource connectionSource = ConnectionSource.instance();

    @Override
    public Optional<Department> getById(BigInteger Id) {

        String query = "SELECT * FROM department WHERE id=?;";
        Department department = null;

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            statement.setInt(1, Id.intValue());
            ResultSet table = statement.executeQuery();

            while(table.next()){
                department = createDepartment(table);
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return Optional.ofNullable(department);
    }

    @Override
    public List<Department> getAll() {

        String query = "SELECT * FROM department;";
        ArrayList<Department> allDepartments = new ArrayList<>();

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            ResultSet table = statement.executeQuery();

            while(table.next()){
                allDepartments.add(createDepartment(table));
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return allDepartments;
    }

    @Override
    public Department save(Department department) {
        String insertQuery = "INSERT INTO department (id, name, location) VALUES (?, ?, ?)";
        String updateQuery = "UPDATE department SET name = ?, location = ? WHERE id = ?;";

        if (getById(department.getId()).isPresent()){

            try(Connection connection = connectionSource.createConnection();
                PreparedStatement statement = connection.prepareStatement(updateQuery) ){

                statement.setString(1, department.getName());
                statement.setString(2, department.getLocation());
                statement.setInt(3, department.getId().intValue());

                statement.execute();

            }catch (SQLException ex){
                ex.printStackTrace();
            }

        } else {

            try(Connection connection = connectionSource.createConnection();
                PreparedStatement statement = connection.prepareStatement(insertQuery) ){

                statement.setInt(1, department.getId().intValue());
                statement.setString(2, department.getName());
                statement.setString(3, department.getLocation());

                statement.execute();

            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }

        return getById(department.getId()).orElse(null);
    }

    @Override
    public void delete(Department department) {
        String query = "DELETE FROM department WHERE id=?;";

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            statement.setInt(1, department.getId().intValue());
            statement.execute();

        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    //HELPER FUNCTIONS
    private Department createDepartment(ResultSet table) throws SQLException{

        BigInteger id = BigInteger.valueOf(table.getInt(1));
        String name = table.getString(2);
        String location = table.getString(3);

        return new Department(id, name, location);
    }
}
