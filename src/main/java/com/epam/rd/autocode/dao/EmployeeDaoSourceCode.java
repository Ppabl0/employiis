package com.epam.rd.autocode.dao;

import com.epam.rd.autocode.ConnectionSource;
import com.epam.rd.autocode.domain.Department;
import com.epam.rd.autocode.domain.Employee;
import com.epam.rd.autocode.domain.FullName;
import com.epam.rd.autocode.domain.Position;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EmployeeDaoSourceCode implements EmployeeDao {

    ConnectionSource connectionSource = ConnectionSource.instance();

    @Override
    public List<Employee> getByDepartment(Department department) {
        ArrayList<Employee> departmentEmployees = new ArrayList<>();
        String query = "SELECT * FROM employee WHERE department=?;";

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            statement.setInt(1, department.getId().intValue());
            ResultSet table = statement.executeQuery();

            while(table.next()){
                departmentEmployees.add(createEmployee(table));
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return departmentEmployees;
    }

    @Override
    public List<Employee> getByManager(Employee employee) {

        ArrayList<Employee> managerEmployees = new ArrayList<>();
        String query = "SELECT * FROM employee WHERE manager=?;";

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            statement.setInt(1, employee.getId().intValue());
            ResultSet table = statement.executeQuery();

            while(table.next()){
                if (isManager(table)) {
                    managerEmployees.add(createEmployee(table));
                }
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return managerEmployees;
    }

    @Override
    public Optional<Employee> getById(BigInteger Id) {
        String query = "SELECT * FROM employee WHERE id=?;";
        Employee employee = null;

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            statement.setInt(1, Id.intValue());
            ResultSet table = statement.executeQuery();

            while(table.next()){
                    employee = createEmployee(table);
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return Optional.ofNullable(employee);

    }

    @Override
    public List<Employee> getAll() {
        String query = "SELECT * FROM employee;";
        ArrayList<Employee> allEmployees = new ArrayList<>();

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            ResultSet table = statement.executeQuery();

            while(table.next()){
                allEmployees.add(createEmployee(table));
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return allEmployees;
    }

    @Override
    public Employee save(Employee employee) {

        String query = "INSERT INTO employee (id, firstname, lastname, middlename, position, manager, hiredate, salary, department ) " +
                       "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            statement.setInt(1, employee.getId().intValue());
            statement.setString(2, employee.getFullName().getFirstName());
            statement.setString(3, employee.getFullName().getLastName());
            statement.setString(4, employee.getFullName().getMiddleName());
            statement.setString(5, employee.getPosition().name());
            statement.setInt(6, employee.getManagerId().intValue());
            statement.setDate(7, Date.valueOf(employee.getHired()));
            statement.setDouble(8, employee.getSalary().doubleValue());
            statement.setInt(9, employee.getDepartmentId().intValue());

            statement.execute();

        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return getById(employee.getId()).orElse(null);

    }

    @Override
    public void delete(Employee employee) {

        String query = "DELETE FROM employee WHERE id=?;";

        try(Connection connection = connectionSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query) ){

            statement.setInt(1, employee.getId().intValue());
            statement.execute();

        }catch (SQLException ex){
            ex.printStackTrace();
        }

    }

    //HELPER FUNCTIONS
    private Employee createEmployee(ResultSet table) throws SQLException{
        BigInteger id = BigInteger.valueOf(table.getInt(1));
        FullName fullName = new FullName(table.getString(2),
                                         table.getString(3),
                                         table.getString(4));
        Position position = Position.valueOf(table.getString(5));
        BigInteger managerId = BigInteger.valueOf(table.getInt(6));
        LocalDate hiredDate = table.getDate(7).toLocalDate();
        BigDecimal salary = table.getBigDecimal(8);
        BigInteger departmentId = BigInteger.valueOf(table.getInt(9));

        return new Employee(id, fullName, position, hiredDate, salary, managerId, departmentId);
    }

    private boolean isManager(ResultSet table) throws SQLException{
        Position position = Position.valueOf(table.getString(5));
         if (position == Position.MANAGER || position == Position.PRESIDENT || position == Position.ANALYST){
             return true;
         } else {
             return false;
         }
    }
}
